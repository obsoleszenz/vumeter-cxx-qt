//! Takes 2 audio inputs and outputs them to 2 audio outputs.
//! All JACK notifications are also printed out.
use rtrb::Consumer;
use rtrb::{RingBuffer};

#[derive(Debug)]
pub enum Message {
    Update(f32)
}

pub fn start_client() -> (impl FnOnce() -> (), Consumer<Message>) {
    // Create client
    let (client, _status) =
        jack::Client::new("rust_jack_simple", jack::ClientOptions::NO_START_SERVER).unwrap();

    // Register ports. They will be used in a callback that will be
    // called when new data is available.
    let in_l = client
        .register_port("rust_in_l", jack::AudioIn::default())
        .unwrap();


    let (mut producer, mut consumer) = RingBuffer::new(2);
    let process_callback = move |_: &jack::Client, ps: &jack::ProcessScope| -> jack::Control {
        let in_l = in_l.as_slice(ps);

        let mut biggest_sample: Option<f32> = None;

        for sample in in_l {
            match biggest_sample {
                None => biggest_sample = Some(sample.clone()),
                Some(current_biggest_sample) => {
                    if sample > &current_biggest_sample {
                        biggest_sample = Some(sample.clone());
                    }
                }
            }
        }

        if let Some(biggest_sample) = biggest_sample {
            producer.push(Message::Update(biggest_sample));
        }


        jack::Control::Continue
    };
    let process = jack::ClosureProcessHandler::new(process_callback);

    // Activate the client, which starts the processing.
    let active_client = client.activate_async(Notifications, process).unwrap();

    let shutdown = move || {
        active_client.deactivate();
    };

    (shutdown, consumer)
}

struct Notifications;

impl jack::NotificationHandler for Notifications {
}
