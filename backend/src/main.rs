use std::io;
use std::thread;

mod lib;
use lib::start_client;

fn main() {
    let (shutdown_client, mut message_queue) = start_client();

    let print_thread = thread::spawn(move || {
        loop {
            if message_queue.is_abandoned() {
                break;
            }
            if let Ok(message) = message_queue.pop() {
                println!("{:?}", message);
            }
        }
    });

    // Wait for user input to quit
    println!("Press enter/return to quit...");
    let mut user_input = String::new();
    io::stdin().read_line(&mut user_input).ok();

    shutdown_client();
    print_thread.join().unwrap();
}

