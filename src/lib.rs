use cxx_qt::make_qobject;

#[make_qobject]
mod my_object {
    use futures::{
        channel::mpsc::{UnboundedReceiver, UnboundedSender},
        executor::block_on,
        FutureExt, StreamExt,
    };
    use std::{
        thread,
    };
    use backend::{start_client, Message};


    #[derive(Default)]
    pub struct Data {
        amplitude: f32,
        peak: f32
    }

    struct RustObj {
        event_sender: UnboundedSender<Message>,
        event_queue: UnboundedReceiver<Message>,
    }
    impl Default for RustObj {
        fn default() -> Self {
            let (event_sender, event_queue) = futures::channel::mpsc::unbounded();
            Self {
                event_sender,
                event_queue,
            }
        }
    }
    impl UpdateRequestHandler<CppObj<'_>> for RustObj {
        fn handle_update_request(&mut self, cpp: &mut CppObj) {
            while let Some(message) = self.event_queue.next().now_or_never() {
                if let Some(message) = message {
                    match message {
                        Message::Update(amplitude) => {
                            cpp.set_amplitude(amplitude);
                            if amplitude > cpp.peak() {
                                cpp.set_peak(amplitude);
                            }
                        }
                    }
                }
            }
        }
    }

    impl RustObj {
        #[invokable]
        fn start_background(&self, cpp: &mut CppObj) {
            println!("Starting background thread...");
            let (shutdown_client, mut message_queue) = start_client();
            let event_sender = self.event_sender.clone();
            let update_requester = cpp.update_requester();

            let background_worker = async move {
                loop {
                    // Simulate the delay of a network request with a simple timer
                    if message_queue.is_abandoned() {
                        break;
                    }
                    if let Ok(message) = message_queue.pop() {
                        let result = event_sender
                            .unbounded_send(message);

                        if result.is_err() {
                            break;
                        }
                    }


                    // Request an update from the background thread
                    update_requester.request_update();
                }
            };
            thread::spawn(move || {
                block_on(background_worker);
                shutdown_client();
            });
        }
    }
}
