import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Window 2.12

import com.kdab.cxx_qt.demo 1.0

Window {
    height: 480
    title: qsTr("Hello test")
    visible: true
    width: 640
    Component.onCompleted: myObject.startBackground()
  

    MyObject {
        id: myObject
        amplitude: null
        peak: null
    }

    Column {
        anchors.fill: parent
        anchors.margins: 10
        spacing: 10

        Label {
            text: "Amplitude: " + myObject.amplitude
        }

        Label {
            text: "Peak: " + myObject.peak
        }

        Rectangle {
          color: "red";
          width: 10;
          height: 10 + myObject.amplitude * 10;
        }
    }
}
