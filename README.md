# vumeter-cxx-qt

VUmeter written with rust + cxx-qt. Mainly a playground to play around with this stack to create an audio application.

# Build

1. Clone the repo
2. `cd` into the cloned repo folder
3. Fetch git submodules with `git submodule update`
4. Build cxx-qt with `cd ./cxx-qt && cmake . build && cd ./build && cmake --build . $(nproc)`
5. Cd back into the root of the repo
6. Build vumeter ui with `cmake . build && cd ./build && cmkae --build . $(nproc)`
